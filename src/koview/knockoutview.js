
import ko from "knockout";
import { getUsers } from "./../logic/business/users";

const html = `
<h1>(knockout) Really pretty users app</h1>
<button data-bind='click: onAdminsClick'>Admins</button>
<button data-bind='click: onNotAdminsClick'>Non Admins</button>
<h1 data-bind="text: isAdmin() ? 'Admins' : 'Non Admins'"></h1>
<div data-bind="if: loading()">
    loading...
</div>
<div data-bind="if: !loading()">
    <table>
        <thead>
            <tr>
                <th>Name</th>
                <th>Email</th>
                <th>Gender</th>
            </tr>
        </thead>
        <tbody data-bind="foreach: users">
            <tr>
                <th data-bind='text: first_name + " " + first_name' ></th>
                <th data-bind='text: email' ></th>
                <th data-bind='text: gender' ></th>
            </tr>
        </tbody>
    </table>
</div>`;

const doKnockoutView = () => {
    ko.components.register('app-component', {
        viewModel: function() {
            this.users = ko.observableArray();
            this.isAdmin = ko.observable(false);
            this.loading = ko.observable(false);

            this.onAdminsClick = () => {
                this.isAdmin(true);
                this.refresh();
            }

            this.onNotAdminsClick = () => {
                this.isAdmin(false);
                this.refresh();
            }

            this.refresh = () => {
                this.loading(true);
                getUsers(this.isAdmin()).then(users => {
                    this.users(users);
                    this.loading(false);
                });
            }

            this.refresh();
        },
        template: html
    });
    
    ko.applyBindings({}, document.getElementById('ko-root'));
}

export default doKnockoutView;