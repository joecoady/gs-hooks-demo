import { useGetUsers } from "./UsersHooks";

type UserProps = {
    isAdmin: boolean
}

const Users = ({ isAdmin }: UserProps) => {
  const usersInfo = useGetUsers(isAdmin);

  return <>
    { usersInfo.isLoading && <div>Loading...</div> }
    { !usersInfo.isLoading && <table>
        <thead>
          <tr>
              <th>Name</th>
              <th>Email</th>
              <th>Gender</th>
          </tr>
        </thead>
        <tbody>
          { usersInfo.data?.map(u => <tr key={u.id}>
              <th>{u.first_name} {u.last_name}</th>
              <th>{u.email}</th>
              <th>{u.gender}</th>
          </tr>) }
        </tbody>
    </table> }
  </>
};

export default Users;