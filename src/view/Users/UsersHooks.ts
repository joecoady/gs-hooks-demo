import { getUsers } from "./../../logic/business/users";
import { useAwait } from "./../platform/platformHooks";

export const useGetUsers = function(isAdmin: boolean) {
    const loadingUsers = useAwait(() => getUsers(isAdmin), [isAdmin]);
    return loadingUsers;
}