import Users from "./../Users/Users"

const NonAdminPanel = () => {
  return <Users isAdmin={false} />
};

export default NonAdminPanel;