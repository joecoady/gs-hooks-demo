
export type CustomLoadingType<T> = {
    data: T | undefined,
    error: Error | undefined,
    isLoading: Boolean,
    isError: Boolean,
    isResolved: Boolean,
};

export const getCustomLoadingStart = <T>(): CustomLoadingType<T> => {
    return {
        data: undefined,
        error: undefined,
        isLoading: true,
        isError: false,
        isResolved: false
    }
}

export const getCustomLoadingResolved = <T>(data: T): CustomLoadingType<T> => {
    return {
        data,
        error: undefined,
        isLoading: false,
        isError: false,
        isResolved: true
    }
}

export const getCustomLoadingErrored = <T>(error: Error): CustomLoadingType<T> => {
    return {
        data: undefined,
        error: error,
        isLoading: false,
        isError: true,
        isResolved: true
    }
}

export const resolveMethodAsPromise = <T>(method: () => T | Promise<T>): Promise<T> => {
    return Promise.resolve(method());
}