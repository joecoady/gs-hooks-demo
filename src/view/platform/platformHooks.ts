import { useState, useEffect } from "react";

import {
    getCustomLoadingStart,
    getCustomLoadingResolved,
    getCustomLoadingErrored,
    resolveMethodAsPromise,
    CustomLoadingType
} from "./platformLogic";


export function useAwait<T>(method: () => T | Promise<T>, deps: Array<any> = []): CustomLoadingType<T> {
    const [ loading, setLoading ] = useState(getCustomLoadingStart<T>());

    useEffect(() => {
        let mounted = true;
        let promise = resolveMethodAsPromise(method);

        promise.then((responseData: T) => {
            if (mounted) {
                setLoading(getCustomLoadingResolved(responseData));
            }
        }).catch((error: Error) => {
            if (mounted) {
                setLoading(getCustomLoadingErrored(error));
            }
        });

        return () => { mounted = false };
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, deps);

    return loading;
}