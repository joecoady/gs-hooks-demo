import { useState } from 'react';

import AdminPanel from './AdminPanel/AdminPanel';
import NonAdminPanel from './NonAdminPanel/NonAdminPanel';

const App = () => {
  const [showAdmin, setShowAdmin ] = useState(false);

  return <div>
    <h1>(React) Really pretty users app</h1>
    <button onClick={e => setShowAdmin(true)}>Admins</button>
    <button onClick={e => setShowAdmin(false)}>Non Admins</button>

    <div>
      { showAdmin && <h1>Admins</h1> }
      { !showAdmin && <h1>Non Admins</h1> }
    </div>
    <div>
    { showAdmin && <AdminPanel /> }
    { !showAdmin && <NonAdminPanel /> }
    </div>
  </div>
};

export default App;