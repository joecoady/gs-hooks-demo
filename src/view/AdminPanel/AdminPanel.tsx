import Users from "./../Users/Users"

const AdminPanel = () => {
  return <Users isAdmin={true} />
};

export default AdminPanel;