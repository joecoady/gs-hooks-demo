export const sleep = function (timeMS: number) : Promise<null> {
    return new Promise((resolve) => {
        setTimeout(() => resolve(null), timeMS)
    });
}
export const apiRequest = async function<T>(url: string) :  Promise<T> {
    const response = await fetch(url);
    const data = await response.json();
    await sleep(1000);
    return data;
}