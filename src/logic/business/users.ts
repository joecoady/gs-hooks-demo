import { apiRequest } from "./../platform/apiRequest"

export type User = {
    id: number,
    first_name: string,
    last_name: string,
    email: string,
    gender: string,
    is_admin: boolean
}

export const getUsers = async function(isAdmin: boolean) :  Promise<Array<User>> {
    const url = isAdmin ? "usersAdmin.json" : "usersNonAdmin.json";
    const users = await apiRequest<Array<User>>(url)
    return users;
}