import './App.css';

const HoloView = ({ viewIndex, children }) => {
    return (
        <div className={`HoloPanel${viewIndex}`}>
            {children}
        </div>
    );
};

export default HoloView;