
import { Suspense, useState } from 'react';
import { Canvas } from '@react-three/fiber'
import Scene from "./Scene/Scene"
import Dropdown from "react-dropdown"
import options from "./options";

import './App.css';

function App() {
  const [model, setModel] = useState(options[0].value);

  return (
    <div className="FullScreen">
      <Suspense fallback={<div>Loading...</div>}>
        <Canvas camera={{ position: [0, 0, 30] }}>
          <Scene model={model} />
        </Canvas>
      </Suspense>
    </div>
  );
}

export default App;
