import { OBJLoader } from 'three/examples/jsm/loaders/OBJLoader'
import { useEffect, useState } from 'react';
import Sphere from "./Sphere";
import options from "./../options"


const useObj = (url) => {
    const [settings, setSettings] = useState([null, 1]);

    useEffect(() => {
        const mounted = true;
        const loader = new OBJLoader();

        setSettings([null, 1]);

        loader.load(
            url,
            function (object) {
                if (mounted) {
                    let { scale, pos, rot, dotPos } = options.find(o => o.value === url);
                    setSettings([object, scale, pos, rot, dotPos]);
                }
            }
        );
    }, [url]);

    return settings;
}

function CustomModel({ model, onClick, visible }) {
    const [group, scale, pos, rot, dotPos] = useObj(model);

    return (
        <>
            {group &&
                <>
                    <mesh onClick={onClick} scale={scale} rotation={rot} position={visible ? pos : [1000, 1000, 1000]}>
                        <primitive object={group} />
                    </mesh>
                    <Sphere position={visible ? dotPos : [1000, 1000, 1000]} visible />
                </>
            }
        </>
    );
}

export default CustomModel;
