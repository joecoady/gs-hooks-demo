import { Canvas } from "react-three-fiber";

import Scene from "./Scene";

function ThreeD({ model }) {
  return (
    <Canvas camera={{ position: [0, 0, 30] }}>
      <Scene model={model} />
    </Canvas>
  );
}

export default ThreeD;
