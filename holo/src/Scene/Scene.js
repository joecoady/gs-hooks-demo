
import { useEffect, useState } from 'react';
import { extend, useThree } from '@react-three/fiber'
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls'
import Sphere from "./Sphere";
import CustomModel from "./CustomModel";

extend({ OrbitControls })

function Scene({ model }) {
  const [toggle, setToggle] = useState(false);
  const [pos, setPos] = useState([0, 0, 0]);
  const [lightPos, setLightPos] = useState([0, 5, 20]);

  const {
    camera,
    gl: { domElement }
  } = useThree();


  const onClick = (e) => {
    setToggle(!toggle);
  };

  useEffect(() => {
    const handleKeyDown = (e) => {
      if (e.keyCode === 32) {
        setLightPos([camera.position.x, camera.position.y, camera.position.z]);
      }
    };
    document.addEventListener("keydown", handleKeyDown);

    return () => {
      document.removeEventListener("keydown", handleKeyDown);
    }
  });

  return (
    <>
      <ambientLight intensity={0.1} />
      <orbitControls args={[camera, domElement]} />
      <pointLight position={lightPos} />
      <pointLight position={[-lightPos[0], lightPos[1], -lightPos[2]]} />

      <CustomModel model={'/skeleton.obj'} onClick={onClick} visible={!toggle} />
      <CustomModel model={'/Brain_Model.obj'} onClick={onClick} visible={toggle} />
    </>
  );
}

export default Scene;
