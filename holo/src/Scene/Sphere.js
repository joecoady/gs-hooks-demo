// Test


function Sphere(props) {

  return (
    <mesh {...props}>
      <sphereGeometry args={[1, 10, 5]} />
      <meshStandardMaterial color={'red'} />
    </mesh>
  );
}

export default Sphere;
