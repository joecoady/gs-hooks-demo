const PI = 3.14159;

const options = [
    { value: '/skeleton.obj', label: 'Skeleton', scale: 1, pos: [0, -20, 0], rot: [0, 0, 0], dotPos: [-1.3982, 1.93658, 2.11919] },
    { value: '/Brain_Model.obj', label: 'Brain', scale: 10, pos: [0, -5, 0], rot: [0, PI / 2, 0], dotPos: [-0.24631945, -1.4780634, 4.2188526] },
    { value: '/john_lambert_head.jpeg.obj', label: 'john lambert', scale: 0.1, pos: [10, 10, 0], rot: [0, 0, PI], dotPos: [-1.3982, 1.93658, 2.11919] },
];

export default options;